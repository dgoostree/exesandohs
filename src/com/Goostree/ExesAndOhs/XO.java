package com.Goostree.ExesAndOhs;

public class XO {
	
	public static boolean getXO (String str) {
	    boolean result = true;
	    
	    if(null != str) {
	    	str = str.toLowerCase();
	    	int len = str.length();
	    	
	    	int xCount = len - str.replaceAll("x", "").length();
	    	int oCount = len - str.replaceAll("o", "").length();
	    	
	    	if(xCount != oCount) {
	    		result = false;
	    	}
	    }	
	    
	    return result;
	}
}
